## Assignment 7-20

1. Sum all integers except the `minimum value` for `minimum sum` and sum all integers except the `maximum value` for the `maximum sum`.
  
      - Given five positive integers, find the `minimum` and `maximum values` that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.

**Example:**
`arr = [9, 1, 5, 7, 3]`

The minimum sum is `1 + 3 + 5 + 7 = 16` and the maximum sum is `3 + 5 + 7 + 9 = 24`.
The output of your function should print 
`min sum: 16`
`max sum: 24`

2. Find the alone integer.

      - Given an array of integers, where all elements but one occur twice, find the unique element.

**Example:**
`arr = [1, 2, 3, 4, 5, 4, 3, 2, 1]`

The unique element is `5`.

The output must be `5`